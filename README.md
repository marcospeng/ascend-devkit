# 简介
华为昇腾开发者套件Atlas 200I DK A2代码仓

# 内容
- 昇腾Atlas 200I DK A2 **系统镜像制作方法**
- 昇腾Atlas 200I DK A2 **开箱入门样例教程**
- 用于Atlas 200I DK A2 SD卡镜像烧录的 **一键制卡工具**
- 用于端侧模型迁移适配的 **模型适配工具**
- 基于昇腾Atlas 200I DK A2开发的 **E2E场景化Demo**
    1. 智能小车
    2. 机械臂
    3. AIoT语音智能家具
    4. ChatBot语音聊天机器人

# 相关链接
华为昇腾开发者套件Atlas 200I DK A2代码仓地址：[https://gitee.com/HUAWEI-ASCEND/ascend-devkit](https://gitee.com/HUAWEI-ASCEND/ascend-devkit)

欢迎使用~

问题、需求反馈渠道：
1. [代码仓提交issues](https://gitee.com/HUAWEI-ASCEND/ascend-devkit/issues)
2. [昇腾论坛Atlas 200I DK A2板块](https://www.hiascend.com/forum/forum-0105101391700280002-1.html)

更多昇腾精彩内容，欢迎访问昇腾社区官网[https://www.hiascend.com](https://www.hiascend.com/)

# 交流群

1. QQ群号：337691047
2. QQ群二维码

![QQ群二维码](https://foruda.gitee.com/images/1681009761430677381/ff1ac171_9317615.png "QQ群")