#!/usr/bin/env python3
# coding: utf-8
import time
import sys
import torch
import rclpy
import Arm_Lib
import cv2 as cv
import numpy as np
from time import sleep
from numpy import random
from dofbot_info.srv import Kinemarics
from cv_bridge import CvBridge, CvBridgeError
from sensor_msgs.msg import Image
from pathlib import Path
import os

from math import pi
from stacking_grap import stacking_grap

from ais_bench.infer.interface import InferSession
from det_utils import letterbox, scale_coords, nms
from npu_utils import get_labels_from_txt, infer_image, draw_prediction, preprocess_image, draw_bbox, xyxy2xywh

cfg = {
    'conf_thres': 0.85,
    'iou_thres': 0.8,
    'input_shape': [640, 640],
}

FILE = Path(__file__).resolve()
ROOT = FILE.parents[0] 
model_folder = os.path.join(ROOT, 'model')
model_path = os.path.join(model_folder, 'yolov5s_bs1.om')
label_path = os.path.join(model_folder, 'coco_names.txt')
cfg_folder = os.path.join(ROOT, 'config')
dp_cfg_path = os.path.join(cfg_folder, 'dp.bin')
offset_cfg_path = os.path.join(cfg_folder, 'offset.txt')
model = InferSession(0, model_path)
labels_dict = get_labels_from_txt(label_path)

class stacking_GetTarget:
    def __init__(self):
        self.image = None
        self.color_name = None
        self.color_status = True
        # 机械臂识别位置调节
        self.xy = [90, 135]
        # 创建机械臂实例
        self.arm = Arm_Lib.Arm_Device()
        # 创建抓取实例
        self.grap = stacking_grap()
        # ROS节点初始化
        rclpy.init(args=sys.argv)
        self.node = rclpy.create_node("dofbot_stacking")
        self.node_pub = rclpy.create_node('dofbot_img_node')
        # 创建获取反解结果的客户端
        self.client = self.node.create_client(Kinemarics, 'trial_service')
        self.garbage_index = 0 # cgz: 用于预热模型
        # 创建ROS发布摄像头图像信息
        self.image_pub = self.node_pub.create_publisher(Image, "cam_data", 10) 
        self.bridge = CvBridge() 
        
        self.offset = -1
        with open(offset_cfg_path, 'r') as f:
            self.offset = float(f.readline())
            print("self.offset is", self.offset) 
        print("finish init..")
        
    def target_run(self, msg, xy=None):
        '''
        抓取函数
        :param msg: (颜色,位置)
        '''
        if xy != None: 
            self.xy = xy
        num = 1
        move_status=0
        
        for i in msg.values():
            if i !=None: 
                move_status=1
                
        if move_status == 1:
            self.arm.Arm_Buzzer_On(1)
            sleep(0.5)
            
        # msg_list = list(msg.items())
        msg_list = sorted(list(msg.items()), key=lambda x: x[1][1])
        for name, pos in msg_list:
            print("pos : ", pos)
            print("name : ",name)
            try:
                joints = self.server_joint(pos)
                self.grap.arm_run(str(num), joints)
                num += 1
            except Exception:
                print("sqaure_pos empty")
                
        # 返回至中心位置
        self.arm.Arm_serial_servo_write(1, 90, 1000)
        sleep(1)
        # 初始位置
        joints_0 = [self.xy[0], self.xy[1], 0, 0, 90, 30]
        # 移动至初始位置
        self.arm.Arm_serial_servo_write6_array(joints_0, 1000)
        sleep(1)

    def select_color(self, image, color_hsv, color_list, mode="MODEL"):
        '''
        选择识别颜色
        :param image:输入图像
        :param color_hsv: hsv的高低阈值
        :param color_list: 颜色序列:['0'：无 '1'：红色 '2'：绿色 '3'：蓝色 '4'：黄色]
        :return: 输出处理后的图像,(颜色,位置)
        '''
        # 规范输入图像大小
        self.image = cv.resize(image, (640, 480))
        msg = {}
        
        if mode == "CV":
            if len(color_list) == 0: 
                return self.image, msg
            if '4' in color_list:
                self.color_name = color_list['4']
                pos = self.get_Sqaure(color_hsv[self.color_name])
                if pos != None: 
                    msg[self.color_name] = pos
            if '3' in color_list:
                self.color_name = color_list['3']
                pos = self.get_Sqaure(color_hsv[self.color_name])
                if pos != None: 
                    msg[self.color_name] = pos
            if '2' in color_list:
                self.color_name = color_list['2']
                pos = self.get_Sqaure(color_hsv[self.color_name])
                if pos != None: 
                    msg[self.color_name] = pos
            if '1' in color_list:
                self.color_name = color_list['1']
                pos = self.get_Sqaure(color_hsv[self.color_name])
                if pos != None: 
                    msg[self.color_name] = pos
        
        elif mode == "MODEL":
            txt0 = 'Model-Loading...'
            msg = {}
            if self.garbage_index < 5: # 模型预热 # revise
                # cv.putText(self.image, txt0, (190, 50), cv.FONT_HERSHEY_SIMPLEX, 1, (0,0,255), 2)
                self.garbage_index += 1
                return self.image, msg 
            if self.garbage_index >= 5:
                # 创建消息容器
                try: 
                    msg = self.get_pos() # 获取识别消息
                except Exception: 
                    print("get_pos NoneType")
                return self.image, msg
        
        return self.image, msg
    
    def get_pos(self):
        '''
        获取识别信息
        :return: 名称,位置
        '''
        # 复制原始图像,避免处理过程中干扰
        img = self.image.copy()
        
        pred, names, drawed_res = infer_image(img, model, labels_dict, cfg)
        self.frame = drawed_res
        if len(pred) >= 1:
            cv.putText(self.frame, "Fix Infer Result, Waiting for Robot Arm Finish..", (30, 50), cv.FONT_HERSHEY_SIMPLEX, 0.8, (0,0,255), 2)
        
        data = self.bridge.cv2_to_imgmsg(drawed_res, encoding="bgr8") 
        self.image_pub.publish(data)
        
        pred = [pred]
        msg = {}
        gn = torch.tensor([640, 480, 640, 480])
        if pred:
            # Process detections
            cgz_ctr = 0
            for i, det in enumerate(pred):  # detections per image
                for *xyxy, conf, cls in reversed(det):
                    prediction_status = True
                    xywh = (xyxy2xywh(torch.tensor(xyxy).view(1, 4)) / gn).view(-1).tolist()  # normalized xywh
                    label = '%s %.2f' % (names[int(cls)], conf)
                    # get name
                    name = names[int(cls)]
                    name = name + str(cgz_ctr)

                    if prediction_status: 
                        point_x = np.int_(xywh[0] * 640)
                        point_y = np.int_(xywh[1] * 480)
                        cv.circle(self.image, (point_x, point_y), 5, (0, 0, 255), -1)
                        # plot_one_box(xyxy, self.image, label=label, color=colors[int(cls)], line_thickness=2)
                        
                        # 计算方块在图像中的位置
                        (a, b) = (round(((point_x - 320) / 4000), 5), round(((480 - point_y) / 3000) * 0.8+0.19, 5))
                        msg[name] = (a, b)
                        cgz_ctr += 1
        return msg


    def get_Sqaure(self, color_hsv):
        '''
        颜色识别,获得方块的坐标
        '''
        (lowerb, upperb) = color_hsv
        # 复制原始图像,避免处理过程中干扰
        mask = self.image.copy()
        # 将图像转换为HSV。
        hsv = cv.cvtColor(self.image, cv.COLOR_BGR2HSV)
        # 筛选出位于两个数组之间的元素。
        img = cv.inRange(hsv, lowerb, upperb)
        # 设置非掩码检测部分全为黑色
        mask[img == 0] = [0, 0, 0]
        # 获取不同形状的结构元素
        kernel = cv.getStructuringElement(cv.MORPH_RECT, (5, 5))
        # 形态学闭操作
        dst_img = cv.morphologyEx(mask, cv.MORPH_CLOSE, kernel)
        # 将图像转为灰度图
        dst_img = cv.cvtColor(dst_img, cv.COLOR_RGB2GRAY)
        # 图像二值化操作
        ret, binary = cv.threshold(dst_img, 10, 255, cv.THRESH_BINARY)
        # 获取轮廓点集(坐标) python2和python3在此处略有不同
        contours, heriachy = cv.findContours(binary, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)  # 获取轮廓点集(坐标)
        for i, cnt in enumerate(contours):
            # boundingRect函数计算边框值，x，y是坐标值，w，h是矩形的宽和高
            x, y, w, h = cv.boundingRect(cnt)
            # 计算轮廓的⾯积
            area = cv.contourArea(cnt)
            # ⾯积限制
            if area > 1000:
                # 中心坐标
                point_x = float(x + w / 2)
                point_y = float(y + h / 2)
                # 在img图像画出矩形，(x, y), (x + w, y + h)是矩形坐标，(0, 255, 0)设置通道颜色，2是设置线条粗度
                cv.rectangle(self.image, (x, y), (x + w, y + h), (0, 255, 0), 2)
                # 绘制矩形中心
                cv.circle(self.image, (int(point_x), int(point_y)), 5, (0, 0, 255), -1)
                # # 在图片中绘制结果
                cv.putText(self.image, self.color_name, (int(x - 15), int(y - 15)),
                           cv.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 255), 2)
                # 计算方块在图像中的位置
                (a, b) = (round(((point_x - 320) / 4000), 5), round(((480 - point_y) / 3000) * 0.8+0.19, 5))
#                 print("------------------ identify up -------------------")
#                 print(a, b)
#                 print("------------------ identify down -------------------")
                return (a, b)

    def server_joint(self, posxy):
        '''
        发布位置请求,获取关节旋转角度
        :param posxy: 位置点x,y坐标
        :return: 每个关节旋转角度
        '''
        # 等待server端启动
        self.client.wait_for_service()
        # 创建消息包
        # request = kinemaricsRequest()
        request = Kinemarics.Request()
        request.tar_x = posxy[0]
        request.tar_y = posxy[1] + self.offset
        request.kin_name = "ik"
        try:
            # response = self.client.call(request)
            self.future = self.client.call_async(request)
            rclpy.spin_until_future_complete(self.node, self.future)
            response = self.future.result()
            if response: 
                # 获得反解响应结果
                joints = [0.0, 0.0, 0.0, 0.0, 0.0]
                joints[0] = response.joint1
                joints[1] = response.joint2
                joints[2] = response.joint3
                joints[3] = response.joint4
                joints[4] = response.joint5
                # 当逆解越界,出现负值时,适当调节.
                if joints[2] < 0:
                    joints[1] += joints[2] * 3 / 5
                    joints[3] += joints[2] * 3 / 5
                    joints[2] = 0
                # print joints
                return joints
        except Exception:
            # rospy.loginfo("arg error")
            print("arg error")


if __name__ == "__main__":
    st_obj = stacking_GetTarget()
    
    capture = cv.VideoCapture(0)
    while capture.isOpened():
        try:
            # 读取相机的每一帧
            _, img = capture.read()
            _, msg = st_obj.select_color(img, 1, 1)
            st_obj.target_run(msg)
        except:
            KeyboardInterrupt:capture.release()