#!/usr/bin/env python
# coding: utf-8

import Arm_Lib
import cv2 as cv
import threading
from time import sleep
from dofbot_config import *
from garbage_identify import garbage_identify
from pathlib import Path

# 创建获取目标实例
target = garbage_identify()
# 创建相机标定实例
calibration = Arm_Calibration()
# 创建机械臂驱动实例
arm = Arm_Lib.Arm_Device()
# 初始化一些参数
num=0
# 初始化标定方框边点
dp = []
# 初始化抓取信息
msg = {}
# 初始化1,2舵机角度值
xy = [90, 135]
# 初始化二值图阈值
threshold = 140

FILE = Path(__file__).resolve()
ROOT = FILE.parents[0] 
cfg_folder = os.path.join(ROOT, 'config')

# XYT参数路径
XYT_path = os.path.join(cfg_folder, 'XYT_config.txt') # revise
try: 
    xy, thresh = read_XYT(XYT_path)
except Exception: 
    print("Read XYT_config Error !!!")
    
warm_up_count = 0
last_num = 0
last_count = 0
    
    
arm = Arm_Lib.Arm_Device()
joints_0 = [xy[0], xy[1], 0, 0, 90, 30] 
arm.Arm_serial_servo_write6_array(joints_0, 1000)


# 打开摄像头
capture = cv.VideoCapture(0)
# 当摄像头正常打开的情况下循环执行
while capture.isOpened():

    # 读取相机的每一帧
    _, img = capture.read()
    # 统一图像大小
    img = cv.resize(img, (640, 480))

    try: 
        write_XYT(XYT_path, [93,130], 140)
    except Exception: 
        print("File XYT_config Error !!!") 
        
    dp = np.array([[86, 31], [18, 426], [609, 427], [537, 31]])
    img = calibration.Perspective_transform(dp, img)    
    
    # ----------  Get Image Index --------------
    img_idx = -1
    with open("/home/HwHiAiUser/RobotArmProject/ros2_dofbot_formal_ws/src/dofbot_garbage_yolov5/dofbot_garbage_yolov5/config/data_collect_idx.txt", "r") as f:
        contents = f.readline()
        # print(type(contents))        
        img_idx = int(contents)
    
    stored_path = "/home/HwHiAiUser/RobotArmProject/ros2_dofbot_formal_ws/src/dofbot_garbage_yolov5/dofbot_garbage_yolov5/data/" + str(img_idx) + ".png"
    cv.imwrite(stored_path, img)
    
    with open("/home/HwHiAiUser/RobotArmProject/ros2_dofbot_formal_ws/src/dofbot_garbage_yolov5/dofbot_garbage_yolov5/config/data_collect_idx.txt", "w") as f:
        img_idx += 1
        f.write(str(img_idx))
    # ---------- Done --------------------------
    break