#!/usr/bin/env python3
# coding: utf-8
import time
import sys
import torch
import rclpy
import Arm_Lib
import cv2 as cv
import numpy as np
from time import sleep
from numpy import random
from dofbot_info.srv import Kinemarics
from cv_bridge import CvBridge, CvBridgeError
from sensor_msgs.msg import Image
from pathlib import Path
import os

from garbage_grap import garbage_grap_move

from ais_bench.infer.interface import InferSession
from det_utils import letterbox, scale_coords, nms
from npu_utils import get_labels_from_txt, infer_image, draw_prediction, preprocess_image, draw_bbox, xyxy2xywh

cfg = {
    'conf_thres': 0.7,
    'iou_thres': 0.7,
    'input_shape': [640, 640],
}

FILE = Path(__file__).resolve()
ROOT = FILE.parents[0] 
model_folder = os.path.join(ROOT, 'model')
model_path = os.path.join(model_folder, 'yolov5s_bs1.om')
label_path = os.path.join(model_folder, 'coco_names.txt')
cfg_folder = os.path.join(ROOT, 'config')
offset_cfg_path = os.path.join(cfg_folder, 'offset.txt')

model = InferSession(0, model_path)
labels_dict = get_labels_from_txt(label_path)
WARMUP_INDEX = 5

class garbage_identify:
    def __init__(self):
        # 初始化图像
        self.frame = None
        # 创建机械臂实例
        self.arm = Arm_Lib.Arm_Device()
        # 机械臂识别位置调节
        self.xy = [90, 130]
        self.garbage_index = 0 
        # 创建垃圾识别抓取实例
        self.grap_move = garbage_grap_move()
        # 创建节点句柄/ROS节点初始化
        rclpy.init(args=sys.argv)
        self.node = rclpy.create_node('dofbot_garbage')
        self.node_pub = rclpy.create_node('dofbot_img_node')
        # 创建用于调用的ROS服务的句柄。
        self.client = self.node.create_client(Kinemarics, 'trial_service')
        # 创建ROS发布摄像头图像信息
        self.image_pub = self.node_pub.create_publisher(Image, "cam_data", 10) 
        self.bridge = CvBridge() 
        
        self.offset = -1
        with open(offset_cfg_path, 'r') as f:
            self.offset = float(f.readline())
            print("self.offset is", self.offset)            
        print("finish init..")

    def garbage_grap(self, msg, xy=None):
        '''
        执行抓取函数
        :param msg: {name:pos,...}
        '''
        if xy != None: 
            self.xy = xy
        if len(msg) != 0:
            self.arm.Arm_Buzzer_On(1)
            sleep(0.5)
        new_msg = sorted(list(msg.items()), key=lambda x: x[1][1]) # cgz add
        print("new msg is", new_msg)
        for elm in new_msg:
            try:
                name = elm[0]
                # 此处ROS反解通讯,获取各关节旋转角度
                joints = self.server_joint(msg[name])
                # 调取移动函数
                self.grap_move.arm_run(str(name), joints)
            except Exception:
                print("sqaure_pos empty")
        # 初始位置
        joints_0 = [self.xy[0], self.xy[1], 0, 0, 90, 30]
        print("back position is", joints_0)
        # 移动至初始位置
        self.arm.Arm_serial_servo_write6_array(joints_0, 1000)
        sleep(1)
        
    def garbage_grap_back(self):
        '''
        执行从红色区域抓取会白色方框的函数
        '''
        self.arm.Arm_Buzzer_On(1)
        sleep(0.5)
        self.grap_move.red_move_back()
        # 初始位置
        joints_0 = [self.xy[0], self.xy[1], 0, 0, 90, 30]
        # 移动至初始位置
        self.arm.Arm_serial_servo_write6_array(joints_0, 1000)
        sleep(1)

    def garbage_run(self, image):
        '''
        执行垃圾识别函数
        :param image: 原始图像
        :return: 识别后的图像,识别信息(name, msg)
        '''
        # 规范输入图像大小
        self.frame = cv.resize(image, (640, 480))
                    
        txt0 = 'Model-Loading...'
        msg = {}
        if self.garbage_index < WARMUP_INDEX: # 模型预热
            cv.putText(self.frame, txt0, (190, 50), cv.FONT_HERSHEY_SIMPLEX, 1, (0,0,255), 2)
            self.garbage_index += 1
            return self.frame, msg 
        if self.garbage_index >= WARMUP_INDEX:
            # 创建消息容器
            try: 
                msg = self.get_pos() # 获取识别消息
                print("msg is:", msg)
            except Exception: 
                print("get_pos NoneType")
            return self.frame, msg
    
    def get_pos(self):
        '''
        获取识别信息
        :return: 名称,位置
        '''
        # 复制原始图像,避免处理过程中干扰
        img = self.frame.copy() 
        
        # print(img.shape)
        # print(labels_dict)
        
        pred, names, drawed_res = infer_image(img, model, labels_dict, cfg)
        self.frame = drawed_res
        # print("pred is:", pred)
        if len(pred) >= 1:
            cv.putText(self.frame, "Fix Infer Result, Waiting for Robot Arm Finish..", (30, 50), cv.FONT_HERSHEY_SIMPLEX, 0.8, (0,0,255), 2)
        
        # bridge = CvBridge() 
        data = self.bridge.cv2_to_imgmsg(drawed_res, encoding="bgr8") 
        self.image_pub.publish(data)
        
        pred = [pred]
        msg = {}
        gn = torch.tensor([640, 480, 640, 480])
        if pred:
            # print("pred is",pred)
            for i, det in enumerate(pred):  # detections per image
                # Write results
                for *xyxy, conf, cls in reversed(det):
                    prediction_status = True
                    xywh = (xyxy2xywh(torch.tensor(xyxy).view(1, 4)) / gn).view(-1).tolist()  # normalized xywh
                    label = '%s %.2f' % (names[int(cls)], conf)
                    # get name
                    name = names[int(cls)]
                    if prediction_status: 
                        point_x = np.int_(xywh[0] * 640)
                        point_y = np.int_(xywh[1] * 480)
                        cv.circle(self.frame, (point_x, point_y), 5, (0, 0, 255), -1)
                        # 计算方块在图像中的位置
                        (a, b) = (round(((point_x - 320) / 4000), 5), round(((480 - point_y) / 3000) * 0.8+0.19, 5))
                        msg[name] = (a, b)
        return msg

    def server_joint(self, posxy):
        '''
        发布位置请求,获取关节旋转角度
        :param posxy: 位置点x,y坐标
        :return: 每个关节旋转角度
        '''
        print("posxy is:", posxy)
        # 等待server端启动
        self.client.wait_for_service(timeout_sec=1.0)
        # 创建消息包
        request = Kinemarics.Request()
        request.tar_x = posxy[0]
        request.tar_y = posxy[1] + self.offset # REVISE
        request.kin_name = "ik"
        try:
            self.future = self.client.call_async(request)
            rclpy.spin_until_future_complete(self.node, self.future)
            response = self.future.result()
            if response:
                # 获取反解的响应结果
                joints = [0, 0, 0, 0, 0]
                joints[0] = response.joint1
                joints[1] = response.joint2
                joints[2] = response.joint3
                joints[3] = response.joint4
                joints[4] = response.joint5
                # 角度调整
                if joints[2] < 0:
                    joints[1] += joints[2] / 2
                    joints[3] += joints[2] * 3 / 4
                    joints[2] = 0
                # print(joints)
                return joints
        except Exception:
            print("arg error")

if __name__ == "__main__":
    print("Enter main..")
    test_mode = "video" # picture # REVISE
    gi_obj = garbage_identify()
    # posxy = [10.0, 10.0]
    # joints = gi_obj.server_joint(posxy)
    # gi_obj.grap_move.arm_run("Syringe", joints)
    
    if test_mode == "video":
        capture = cv.VideoCapture(0)
        while capture.isOpened():
            try:
                # 读取相机的每一帧
                _, img = capture.read()
                gi_obj.garbage_run(img)
            except:
                KeyboardInterrupt:capture.release()
    elif test_mode == "picture":
        count = 0
        while count  < 4:
            # infer_image(img_path, model, labels_dict, cfg)
            img_bgr = cv.imread(img_path)
            gi_obj.garbage_run(img_bgr)