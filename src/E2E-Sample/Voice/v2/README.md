# 智能语音台灯样例

## 1 样例介绍
智能语音台灯应用通过将语音识别模型WeNet转换为OM模型，使其能够运行在Atlas 200I DK A2开发者套件上的昇腾AI处理器进行加速，主要的工作流程是在网页前端收集用户语音输入，通过后端放入消息队列，后端进行语音识别模型推理解析为文本，并转换为控制命令发送给台灯，从而实现台灯开关控制。交互界面以聊天窗口的形式呈现，用户通过语音输入命令后，聊天机器人会以文本的形式返回控制结果。

## 2 依赖文件
你可以从[这个链接](https://ascend-repo.obs.cn-east-2.myhuaweicloud.com/Atlas%20200I%20DK%20A2/DevKit/samples/23.0.RC1/e2e-samples/Voice/e2e_voice_lamp.zip)获取依赖文件，并整理成如下结构：
```
├── config.py    
├── dist
│   ├── assets
│   │   ├── avtor-141cd8e9.jpg
│   │   ├── index-0c2196b0.css
│   │   ├── index-0f38e264.js
│   │   └── me-f369eebc.jpg
│   ├── index.html
│   └── vite.svg
├── main.py
└── wenet
    ├── model.py
    ├── offline_encoder.om
    └── vocab.txt
```

## 3 模型转换
> 第二步提供的依赖文件中已包含转换后的om模型，可以直接使用。
* **建议在Linux服务器或者虚拟机转换该模型**
* 首先我们可以在[这个链接](https://ascend-repo.obs.cn-east-2.myhuaweicloud.com/Atlas%20200I%20DK%20A2/DevKit/samples/23.0.RC1/base-samples/notebook-demo-datasets/10-speech-recognition/offline_encoder_sim.onnx)下载我们已经准备好了的onnx模型文件，ONNX是开源的离线推理模型框架。
* 为了能进一步优化模型推理性能，我们需要将其转换为om模型进行使用，以下为转换指令：  
    ```shell
    atc --model=offline_encoder_sim.onnx --framework=5 --output=offline_encoder --input_format=ND --input_shape="speech:1,1478,80;speech_lengths:1" --log=error --soc_version=Ascend310B1
    ```
    * 其中转换参数的含义为：  
        * --model：输入模型路径
        * --framework：原始网络模型框架类型，5表示ONNX
        * --output：输出模型路径
        * --input_format：输入Tensor的内存排列方式
        * --input_shape：指定模型输入数据的shape。这里我们在转模型的时候指定了最大的输入音频长度，推荐的长度有：262,326,390,454,518,582,646,710,774,838,902,966,1028,1284,1478，默认使用的长度是1478
        * --log：日志级别
        * --soc_version：昇腾AI处理器型号

## 4 硬件连接以及样例运行
> 模型推理依赖 ais_bench 推理工具，在基础镜像中已安装。若环境中未安装该工具，请参照[对应的代码仓](https://gitee.com/ascend/tools/tree/master/ais-bench_workload/tool/ais_bench)说明进行安装。

请在开发者套件详情页点击智能语音台灯应用开发指南了解该部分详细内容。

## 5 [物料清单链接](https://ascend-imager-repo.obs.cn-south-1.myhuaweicloud.com/E2E/AIoT/0.0.2/E2E%E8%AF%AD%E9%9F%B3AIoT%E6%A0%B7%E4%BE%8B%E7%89%A9%E6%96%99%E6%B8%85%E5%8D%95.docx)