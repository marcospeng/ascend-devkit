import torchaudio
import torchaudio.compliance.kaldi as kaldi
from ais_bench.infer.interface import InferSession
import numpy as np
from swig_decoders import map_batch


class WeNetASR:
    def __init__(self, model_path, vocab_path):
        self.vocabulary = load_vocab(vocab_path)
        self.model = InferSession(0, model_path)
        self.max_len = self.model.get_inputs()[0].shape[1]

    def transcribe(self, wav_file):
        """执行模型推理，将录音文件转为文本。"""
        feats_pad, feats_lengths = self.preprocess(wav_file)
        output = self.model.infer([feats_pad, feats_lengths])
        txt = self.post_process(output)
        return txt

    def preprocess(self, wav_file):
        """数据预处理"""
        waveform, sample_rate = torchaudio.load(wav_file)
        # 音频重采样，采样率16000
        waveform, sample_rate = resample(waveform, sample_rate, resample_rate=16000)
        # 计算fbank特征
        feature = compute_fbank(waveform, sample_rate)
        feats_lengths = np.array([feature.shape[0]]).astype(np.int32)
        # 对输入特征进行padding，使符合模型输入尺寸
        feats_pad = pad_sequence(feature,
                                 batch_first=True,
                                 padding_value=0,
                                 max_len=self.max_len)
        feats_pad = feats_pad.numpy().astype(np.float32)
        return feats_pad, feats_lengths

    def post_process(self, output):
        """对模型推理结果进行后处理"""
        encoder_out, encoder_out_lens, ctc_log_probs, \
            beam_log_probs, beam_log_probs_idx = output
        batch_size = beam_log_probs.shape[0]

        num_processes = batch_size
        log_probs_idx = beam_log_probs_idx[:, :, 0]
        batch_sents = []
        for idx, seq in enumerate(log_probs_idx):
            batch_sents.append(seq[:encoder_out_lens[idx]].tolist())
        # 根据预置的标签字典将推理结果转换为文本
        txt = map_batch(batch_sents, self.vocabulary, num_processes, True, 0)[0]
        return txt


def pad_sequence(seq_feature, batch_first=True, padding_value=0, max_len=966):
    feature_shape = seq_feature.shape
    feat_len = feature_shape[0]
    if feat_len > max_len:
        # truncate to max length
        seq_feature = seq_feature[:max_len].unsqueeze(0)
        return seq_feature

    batch_size = 1
    trailing_dims = feature_shape[1:]
    if batch_first:
        out_dims = (batch_size, max_len) + trailing_dims
    else:
        out_dims = (max_len, batch_size) + trailing_dims

    out_tensor = seq_feature.data.new(*out_dims).fill_(padding_value)
    if batch_first:
        out_tensor[0, :feat_len, ...] = seq_feature
    else:
        out_tensor[:feat_len, 0, ...] = seq_feature
    return out_tensor


def resample(waveform, sample_rate, resample_rate=16000):
    waveform = torchaudio.transforms.Resample(
        orig_freq=sample_rate, new_freq=resample_rate)(waveform)
    return waveform, resample_rate


def compute_fbank(waveform,
                  sample_rate,
                  num_mel_bins=80,
                  frame_length=25,
                  frame_shift=10,
                  dither=0.0):
    """ Extract fbank features"""
    waveform = waveform * (1 << 15)
    mat = kaldi.fbank(waveform,
                      num_mel_bins=num_mel_bins,
                      frame_length=frame_length,
                      frame_shift=frame_shift,
                      dither=dither,
                      energy_floor=0.0,
                      sample_frequency=sample_rate)
    return mat


def load_vocab(txt_path):
    vocabulary = []
    with open(txt_path, 'r') as fin:
        for line in fin:
            arr = line.strip().split()
            assert len(arr) == 2
            vocabulary.append(arr[0])
    return vocabulary
