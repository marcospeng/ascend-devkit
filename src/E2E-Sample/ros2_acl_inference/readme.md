# ROS-ACL：面向ROS2的AscendCL推理框架

## 功能介绍

ROS-ACL是针对昇腾AI开发者套件[Atlas 200I DK A2](https://www.hiascend.com/hardware/developer-kit-a2)的板端AI推理框架，基于CANN AscendCL接口和ROS2 Node进行二次开发，为机器人应用开发提供简单易用的调用接口和实践范例，让开发者能快速在Atlas 200I DK A2上利用Ascend 310A2 AI处理器的强大算力和图像处理能力实现图像分类、物体检测等AI功能。

本代码仓是ROS Package的形式，可在板端ROS2工作空间中直接编译运行。它包含一个模型推理节点inference_node。usb摄像头按一定帧率以ROS msg的形式发布/image_raw图像话题；inference_node订阅/image_raw话题，对接收到的图像做推理，并将结果以ROS msg的形式发布（供下游任务处理）。

代码主要包含两部分内容：


- AscendCL推理基类NNBaseNode：将推理应用中涉及到的AscendCL初始化/去初始化、运行管理资源申请/释放、模型推理等公共步骤进行封装。

- 用户派生类NNObjectDetectionNode：以目标检测应用为样例，通过继承NNBaseNode类，添加参数配置和对模型输入输出的处理，实现完整的推理任务。

整个图像目标检测功能包的节点分布图如下所示：
 ![objection_nodes_picture.png](objection_nodes_picture.png)

## 目录结构

图像检测样例代码结构如下所示。

```
├── include
│   ├── AclInterfaceImpl.h       // ACL推理实现类声明文件
│   ├── NNBaseNode.h             // ACL推理流程虚基类声明头文件
│   ├── NNObjectDetectNode.h                     // 目标检测子类声明头文件
│   ├── utils.h                  // 功能函数声明头文件

├── launch
│   ├── nodes.launch.py          // 用于启动多个节点和配置节点参数的launch文件

├── src
│   ├── AclInterfaceImpl.cpp     // ACL推理实现类实现源文件
│   ├── detection_example.cpp    // 目标检测样例推理节点源文件
│   ├── NNBaseNode.cpp           // ACL推理流程虚基类实现源文件
│   ├── NNObjectDetectNode.cpp   // 目标检测子类实现源文件
│   ├── utils.cpp                // 功能函数实现源文件

├── CMakeLists.txt               // ROS2功能包编译脚本
├── package.xml                  // 配置文件，添加节点依赖项
├── readme.md                    // 说明文件，包含代码逻辑及运行说明
```

## 前置条件

ROS-ACL是面向开发者用Atlas 200I DK A2做机器人AI应用开发的，使用前需先将特定的系统镜像烧录至SD卡，详细制卡操作请参考[制卡教程](https://www.hiascend.com/hardware/developer-kit-a2/resource)。使用本代码仓前请检查以下环境条件是否满足，如不满足请按照备注进行相应处理。
| 条件 | 要求 | 备注|
|---|---|---|
| CANN | >=6.2 | SD卡的系统镜像中已预装CANN，注意固件与驱动版本 |
| 操作系统 | Ubuntu 22.04 | SD卡的系统镜像默认即是Ubuntu 22.04 |
| ROS2 | Humble | 请参考[ROS2官网](http://docs.ros.org/en/humble/Installation.html)的教程完成安装 |
| 第三方依赖 | OpenCV 4.5.4 | 请参考[第三方依赖安装指导](https://gitee.com/ascend/samples/blob/master/cplusplus/environment/catenation_environmental_guidance_CN.md)完成安装 |

## 样例运行

1、摄像头发布节点启动

（1）连接usb摄像头

将摄像头的usb接头插到小站的usb接头上。

（2）查看usb摄像头是否连接成功

在小站ubuntu终端运行lsusb指令，当出现类似如下打印信息，即可说明连接成功
```
Bus 001 Device 002: ID 046d:085c Logitech, Inc. C922 Pro Stream Webcam
```
（3）安装ros2下摄像头启动需要的依赖
- 安装相关功能包
```
sudo apt install ros-humble-camera-calibration-parsers
sudo apt install ros-humble-camera-info-manager
sudo apt install ros-humble-launch-testing-ament-cmake
```
- ros2-cam驱动安装
```
sudo apt-get install ros-humble-usb-cam
```
- cv2安装
```
pip install opencv-python
```
（4）启动摄像头节点
```
ros2 launch usb_cam demo_launch.py
```
看到类似如下信息打印，即可说明摄像头图像话题（/image_raw）发布节点启动成功，节点话题发布频率为30hz左右。
```
[usb_cam_node_exe-1] [INFO] [1682650739.270560029] [usb_cam]: Timer triggering every 33 ms
```

2、ros2_acl_inference图像目标检测节点启动

（1）创建工作空间
```
mkdir -p dev_ws/src
```

（2）创建功能包

进入到dev_ws/src文件夹，从代码仓里面下载ros2_acl_inference目录下样例代码。

（3）下载yolov3.om模型文件

进入ros2_acl_inference文件夹，新建一个名为model文件夹，通过以下网址直接下载图像目标检测对应的.om文件，放入model文件夹中，网址如下所示：
```
https://ascend-repo.obs.cn-east-2.myhuaweicloud.com/Atlas%20200I%20DK%20A2/DevKit/models/23.0.RC1/cann/yolov3.om
```

（4）rosdep检查依赖安装完整性
```
rosdep install -i --from-path src --rosdistro humble –y
```

（5）回退到dev_ws主文件夹，colcon编译功能包

在launch/nodes.launch.py中配置图像发布节点和模型推理节点的相关参数，然后执行以下指令：
```
colcon build --packages-select ros2_acl_inference
```

（6）打开新终端，设置工作空间的环境变量后，运行launch文件，启动inference_node图像目标检测节点
```
source install/setup.bash
ros2 launch ros2_acl_inference nodes.launch.py
```

## 查看图像目标检测结果

节点启动后，可在终端看到ROS消息收发、图像处理、模型推理相关的打印信息。另外可另开新的终端打开rviz2，通过添加/detection_result话题查看目标检测的结果。操作步骤如下所示：

打开一个新终端，键入rviz2指令，即可打开rviz2界面，然后点击左下角的Add按钮，然后选择By topic，找到/detection_result话题中的image，就可以看到实时图像目标检测的画面，带框和相似度百分比的，如下所示。

 ![rviz2_display_topic.png](rviz2_display_topic.png)


## 注意事项

1、可以通过ros2 run的方式分别启动图像发布节点和模型推理节点，但需要在源代码中将launch文件里对应的参数配置好；

2、如果在安装、编译、运行过程中遇到CANN、AscendCL等相关问题，可参考[常见问题定位](https://gitee.com/ascend/samples/wikis/%E5%B8%B8%E8%A7%81%E9%97%AE%E9%A2%98%E5%AE%9A%E4%BD%8D/%E4%BB%8B%E7%BB%8D)；
