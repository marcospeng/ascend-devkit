import os
import torch
from tqdm import tqdm
import torch.nn as nn
import torch.nn.functional as F
import torchvision.transforms as transforms
import torchvision.models as models

from torchvision.transforms.functional import resized_crop
import random
from data import Dataset
# from model import Net
import configparser

# 加载配置文件
config = configparser.ConfigParser()
cur_dir_path = os.path.dirname(os.path.realpath(__file__))
cfg_dir_path = os.path.join(cur_dir_path, "config")
cfg_file_path = os.path.join(cfg_dir_path, 'train_setting.ini')
config.read(cfg_file_path)
IF_GPU = config['DEFAULT'].getboolean('IF_GPU')
NEED_TRAIN = config['DEFAULT'].getboolean('NEED_TRAIN')
KEEP_TRAIN = config['DEFAULT'].getboolean('KEEP_TRAIN')
TO_ONNX = config['DEFAULT'].getboolean('TO_ONNX')
EPOCH_NUM = int(config['DEFAULT']['EPOCH_NUM'])
BATCH_SIZE = int(config['DEFAULT']['BATCH_SIZE'])
DATA_DIR = config['DEFAULT']['DATA_DIR']
IMAGE_DIR = os.path.join(DATA_DIR, "images") 
OUT_MODEL_PATH = config['DEFAULT']['OUT_MODEL_PATH']
ONNX_OUTPUT_FOLDER = config['DEFAULT']['ONNX_OUTPUT_FOLDER']
ONNX_FILE_NAME = config['DEFAULT']['ONNX_FILE_NAME']
LOG_PATH = config['DEFAULT']['LOG_PATH']

# file_list = os.listdir(DATA_DIR)

def crop_upper(image):
  return resized_crop(image, 170, 0, 342, 512, (66,200))

if __name__ == '__main__':
    brightness_change = transforms.ColorJitter(brightness=0.5)
    contrast_change = transforms.ColorJitter(contrast=0.5)
    color_aug = transforms.ColorJitter(brightness=0.5, contrast=0.5, saturation=0.5, hue=0.5)
    size = random.choice([1,3,5,7])  
    transform = transforms.Compose(
      [
        ## Optional Ones
        brightness_change,
        contrast_change,
        transforms.GaussianBlur(kernel_size=(size, size)),
        
        ## Must
        transforms.Lambda(crop_upper),
        transforms.ToTensor(),
      ]
    )

    dataset = Dataset(root=DATA_DIR, transform=transform)
    print("length of dataset is", len(dataset))
    train_size = int(0.9 * len(dataset))
    print("train_size of dataset is", train_size)
    test_size = len(dataset) - train_size
    trainset, testset = torch.utils.data.random_split(dataset, [train_size, test_size])
    
    trainloader = torch.utils.data.DataLoader(trainset, batch_size=BATCH_SIZE,
                                            shuffle=True, num_workers=0, drop_last=True)

    testloader = torch.utils.data.DataLoader(testset, batch_size=1,
                                            shuffle=False, num_workers=0)
    
    model = models.resnet18(pretrained=True)
    model.fc = torch.nn.Linear(512, 1)
    if IF_GPU:
      net = model.cuda()
    else:
      net = model

    criterion = nn.MSELoss()
    optimizer = torch.optim.Adam(net.parameters(), lr=0.001)


    if NEED_TRAIN:
        if KEEP_TRAIN:
            net.load_state_dict(torch.load(OUT_MODEL_PATH))
            print("finish loading..")
            
        for epoch in range(EPOCH_NUM):  # loop over the dataset multiple times

            running_loss = 0.0
            for i, data in enumerate(tqdm(trainloader)):
                # get the inputs; data is a list of [inputs, labels]
                inputs, labels = data
                if IF_GPU:
                  inputs=inputs.cuda()
                  labels=labels.cuda()
                labels = labels.unsqueeze(1)

                # zero the parameter gradients
                optimizer.zero_grad()

                # forward + backward + optimize
                outputs = net(inputs)
                loss = criterion(outputs, labels)
                loss.backward()
                optimizer.step()

                # print statistics
                running_loss += loss.item()
                
            print(f'epoch: {epoch} training loss: {running_loss /(i + 1)}')
            with open(LOG_PATH, "a", encoding='utf-8') as f:
              f.write(f'epoch: {epoch} training loss: {running_loss /(i + 1)}')
              f.write(f'\n')

            
            val_loss = 0.
            for i,data in enumerate(tqdm(testloader)):
                inputs, labels = data
                if IF_GPU:
                  inputs=inputs.cuda()
                  labels=labels.cuda()
                
                # zero the parameter gradients
                optimizer.zero_grad()

                # forward + backward + optimize
                outputs = net(inputs)
                loss = criterion(outputs, labels)
                val_loss += loss.item()
                
            print(f'epoch: {epoch} validation loss: {val_loss /(i + 1)}')
            with open(LOG_PATH, "a", encoding='utf-8') as f:
              f.write(f'epoch: {epoch} validation loss: {val_loss /(i + 1)}')
              f.write(f'\n')

        f.close()
        print('Finished Training')

        ## 保存模型
        torch.save(net.state_dict(), OUT_MODEL_PATH)
    else:
        ## 加载模型
        net.load_state_dict(torch.load(OUT_MODEL_PATH))



    if IF_GPU:
      x = torch.randn(1, 3, 66, 200, requires_grad=True).cuda() # REVISE
    else:
      x = torch.randn(1, 3, 66, 200, requires_grad=True)
    torch_out = net(x)
    print("After testing.., torch_out has shape:", torch_out)
    
    if TO_ONNX:
      tmp_dir = ONNX_OUTPUT_FOLDER # REVISE
      input_names = ["actual_input_1"]
      output_names = ["output1"]
      dynamic_axes = {'actual_input_1': {0: '-1'}, 'output1': {0: '-1'}}
      if IF_GPU:
        dummy_input = torch.randn(1, 3, 66, 200).cuda() # REVISE
      else:
        dummy_input = torch.randn(1, 3, 66, 200)
      torch.onnx.export(net, dummy_input, os.path.join(tmp_dir, ONNX_FILE_NAME) , input_names = input_names, dynamic_axes = dynamic_axes, output_names = output_names, opset_version=11) # REVISE