import torch
import os
import cv2
from torchvision.datasets import ImageFolder
import collections
from PIL import Image
import numpy as np

class Dataset(ImageFolder):
    def __init__(self, root, transform=None) -> None:
        super().__init__(root=root, transform=transform)
        self.labels = collections.defaultdict(int)
        self.samples = []
        for file_name in os.listdir(os.path.join(root, 'images')):
            self.samples.append(os.path.join(root, 'images', file_name))
        with open(os.path.join(root, 'labels.txt'),'r') as f:
            for l in f.readlines():
                basename, label = l.split(' ')
                self.labels[basename] = float(label)
                
    def __len__(self):
        return len(self.samples)
    
    def __getitem__(self, index):
        path = self.samples[index]
        basename = os.path.basename(path)
        target = self.labels[basename]
        sample = self.loader(path)
        sample = cv2.cvtColor(np.asarray(sample),cv2.COLOR_RGB2BGR)
        sample = Image.fromarray(cv2.cvtColor(sample,cv2.COLOR_BGR2RGB))
        if self.transform is not None:
            sample = self.transform(sample)
        if self.target_transform is not None:
            target = self.target_transform(target)
        else:
            target = torch.tensor(target)
        return sample, target