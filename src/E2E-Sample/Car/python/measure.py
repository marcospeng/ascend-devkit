from src.actions import TurnRight, TurnLeft, Stop
from src.utils import Controller

if __name__ == '__main__':
    ctrl = Controller()
    while True:
        keys = input().strip().split()
        print(keys)
        if len(keys) > 1:
            value = float(keys[1])
        else:
            ctrl.execute(Stop())
        key = keys[0]
        if key.lower() == 'l':
            ctrl.execute(TurnLeft(speed=25, degree=value))
        elif key.lower() == 'r':
            ctrl.execute(TurnRight(speed=25, degree=value))
