#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import time

import numpy as np

from src.actions import SetServo, Stop, TurnLeftInPlace, TurnRightInPlace, Parking,TurnAround
from src.models import YoloV5, DetCls
from src.scenes.base_scene import BaseScene
from src.utils import log


class Helper(BaseScene):
    def __init__(self, memory_name, camera_info, msg_queue):
        super().__init__(memory_name, camera_info, msg_queue)
        self.det = None
        self.cls = None

    def init_state(self):
        log.info(f'start init {self.__class__.__name__}')
        det_path = os.path.join(os.getcwd(), 'weights', 'yolo.om')
        cls_path = os.path.join(os.getcwd(), 'weights', 'cls.om')
        if not os.path.exists(det_path) or not os.path.exists(cls_path):
            log.error(f'Cannot find the offline inference model(.om) file needed for {self.__class__.__name__}  scene.')
            return True
        self.det = YoloV5(det_path)
        self.cls = DetCls(cls_path, acl_init=False)
        log.info(f'{self.__class__.__name__} model init succ.')
        self.ctrl.execute(SetServo(servo=[90, 65]))
        return False

    def loop(self):
        ret = self.init_state()
        if ret:
            log.error(f'{self.__class__.__name__} init failed.')
            return
        frame = np.ndarray((self.height, self.width, 3), dtype=np.uint8, buffer=self.broadcaster.buf)
        log.info(f'{self.__class__.__name__} loop start')
        # self.ctrl.execute(Start())
        try:
            while True:
                if self.stop_sign.value:
                    break
                if self.pause_sign.value:
                    continue
                start = time.time()
                img_bgr = frame.copy()
                bboxes = self.det.infer(img_bgr)
                log.info(f'{bboxes}')

                for x1, y1, x2, y2, cate, score in bboxes:
                    x, y = (x1 + x2) // 2, (y1 + y2) // 2
                    h, w = y2 - y1, x2 - x1
                    sub_img = img_bgr[y1:y2, x1:x2]
                    cate1 = self.cls.infer(sub_img)
                    log.info(f'det: {cate} cls:{cate1}')

                    if cate == 'left' and cate1 == 'left':
                        if 300 < x < 700 and y >= 530:
                            self.ctrl.execute(TurnLeftInPlace())
                            time.sleep(1.6)
                            break

                    if cate == 'left' and cate1 != 'left':
                        if 420 < x < 950 and y >= 580:
                            self.ctrl.execute(TurnRightInPlace())
                            time.sleep(1)
                            break

                    if (cate == 'right' or cate == 'left') and cate1 == 'stop':
                        if x1 > 850 and w >= 100 and y1 > 200:
                            self.ctrl.execute(Parking())
                            time.sleep(1.6)
                            break

                    if cate == 'turnaround':
                        if 150 < x < 400 and w >= 120 and y1 > 200:
                            self.ctrl.execute(TurnAround())
                            time.sleep(3)
                            break
                log.info(f'infer cost {time.time() - start}')
        except KeyboardInterrupt:
            self.ctrl.execute(Stop())
