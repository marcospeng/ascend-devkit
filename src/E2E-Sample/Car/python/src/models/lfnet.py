import numpy as np

from src.models.bsae_model import Model


class LFNet(Model):
    def __init__(self, model_path, acl_init=True):
        super().__init__(model_path, acl_init)

    def infer(self, inputs):
        img_bgr = inputs[:, :, [2, 1, 0]]
        np_image = np.array(img_bgr).astype('float32') / 255.0
        batched_img = np.ascontiguousarray(np.expand_dims(np_image, axis=0))
        result = self.execute([batched_img])
        return result
