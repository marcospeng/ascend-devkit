# ChatBot聊天机器人

本项目是基于ChatYuan-Large-V1，部署于Atlas 200I DK A2的对话机器人。

原始项目链接：[ChatYuan-Large-V1](https://huggingface.co/ClueAI/ChatYuan-large-v1)

## Demo运行代码详见DEMO目录

## 模型转换流程详见ModelConvert目录