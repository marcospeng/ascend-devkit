#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from multiprocessing import Process, Queue

from flask import Flask, request, jsonify
from flask import render_template  # 引入模板插件
from flask_cors import CORS

from generate import T5Model


class ChatBot:
    def run(self, input_queue, output_queue):
        model = T5Model()
        print('ChatBot Ready')
        while True:
            if input_queue.empty():
                continue
            input_text = input_queue.get()
            model.generate_ss_mode(input_text, output_queue)


bot = ChatBot()
in_q = Queue(2)
out_q = Queue(1)
process = Process(target=bot.run, args=(in_q, out_q))
process.start()

if __name__ == '__main__':

    app = Flask(
        __name__,
        static_folder='./dist_chatbot_standalone',  # 设置静态文件夹目录
        template_folder="./dist_chatbot_standalone",
        static_url_path=""
    )

    CORS(app, resources=r'/*')


    @app.route('/')
    def index():
        return render_template('index.html', name='index')


    @app.route("/chat", methods=["GET"])
    def getChat():
        args = request.args
        message = args.get("message")
        print('msg received')
        in_q.put(message)
        data = {
            "code": 200,
            "message": ''
        }
        return jsonify(data)


    @app.route("/getMsg", methods=["GET"])
    def getMsg():
        if out_q.empty():
            data = {
                "code": 200,
                "data": {
                    "isEnd": False,
                    "message": ""
                }
            }

        else:
            data = out_q.get()
            print(data)
        return jsonify(data)


    app.run(
        host="0.0.0.0",
        port=5000
    )
