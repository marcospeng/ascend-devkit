# ChatBot聊天机器人DEMO运行指南

相关资源的下载地址列表如下

| 资源名称                    | 下载地址 |
|:------------------------| :--- |
| dist_chatbot_standalone | [https://ascend-repo.obs.cn-east-2.myhuaweicloud.com/Atlas%20200I%20DK%20A2/DevKit/samples/23.0.RC1/e2e-samples/ChatBot/dist_chatbot_standalone.zip](https://ascend-repo.obs.cn-east-2.myhuaweicloud.com/Atlas%20200I%20DK%20A2/DevKit/samples/23.0.RC1/e2e-samples/ChatBot/dist_chatbot_standalone.zip) |
| models                  | [https://ascend-repo.obs.cn-east-2.myhuaweicloud.com/Atlas%20200I%20DK%20A2/DevKit/samples/23.0.RC1/e2e-samples/ChatBot/models.zip](https://ascend-repo.obs.cn-east-2.myhuaweicloud.com/Atlas%20200I%20DK%20A2/DevKit/samples/23.0.RC1/e2e-samples/ChatBot/models.zip) |
| tokenizer_file          | [https://ascend-repo.obs.cn-east-2.myhuaweicloud.com/Atlas%20200I%20DK%20A2/DevKit/samples/23.0.RC1/e2e-samples/ChatBot/tokenizer_file.zip](https://ascend-repo.obs.cn-east-2.myhuaweicloud.com/Atlas%20200I%20DK%20A2/DevKit/samples/23.0.RC1/e2e-samples/ChatBot/tokenizer_file.zip) |

## 0.环境准备
**注意: 在4G版本的设备上运行时，需要挂载swap分区，否则可能会出现内存不足的情况。**

**请在运行前提前挂载swap分区**

流程如下

### 1.通过free -h命令查看内存使用情况，如果内存总量小于4G，则需要挂载swap分区
```angular2html
free -h
```
### 2.申请一个2.5G的文件作为swap分区【推荐2.5G以上，请提前预留足够的空间】
```angular2html
sudo fallocate -l 2.5G /swapfile 
```

### 3.修改文件权限
```angular2html
sudo chmod 600 /swapfile
```

### 4.创建swap分区
```angular2html
sudo mkswap /swapfile
```

### 5.挂载swap分区
```angular2html
sudo swapon /swapfile
```

### 6.通过 free -h查看swap分区是否挂载成功
```angular2html
free -h
```

## 1.安装依赖

```angular2html
pip install -r requirements.txt
```

## 2.下载相关资源并放入对应目录
请将dist_chatbot_standalone.zip、models.zip、tokenizer_file.zip解压后放入对应目录

## 3.运行DEMO

```angular2html
python main.py
```

## 4.获取命令行回显URL并在浏览器中打开

**当命令行回显ChatBot Ready时，表示初始化完成，DEMO运行成功**


**提示：在聊天框中输入clear可以清空聊天记录**

**注意：如在4G内存设备上推理，前2-3次推理会消耗较多时间进行warmup**
