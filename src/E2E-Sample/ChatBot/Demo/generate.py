#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import numpy as np
import onnxruntime
import torch
import torch.nn as nn
from ais_bench.infer.interface import InferSession
from transformers import T5Tokenizer
from transformers.generation import LogitsProcessorList, NoRepeatNGramLogitsProcessor, TemperatureLogitsWarper, \
    TopKLogitsWarper, StoppingCriteriaList, MaxLengthCriteria

from utils import preprocess, postprocess, content_generate, generate_onnx_input


class T5Model:
    def __init__(self):
        print('start init encoder. It will cost a few minutes')
        self.encoder = InferSession(0, './models/encoder.om')
        print('The encoder has been initialized. Initializing the first decoder in progress.')
        self.first_decoder = onnxruntime.InferenceSession('./models/decoder_first_sim_quant.onnx')
        print('The first decoder has been initialized. Initializing the second decoder in progress.')
        self.decoder_iter = onnxruntime.InferenceSession('./models/decoder_iter_sim_quant.onnx')
        print('The second decoder has been initialized.')

        self.tokenizer = T5Tokenizer.from_pretrained("./tokenizer_file")
        self.dummy_decoder_input_ids = np.array([[0]], dtype=np.int64)
        self.logits_processor = LogitsProcessorList([NoRepeatNGramLogitsProcessor(3)])
        self.logits_warper = LogitsProcessorList(
            [TemperatureLogitsWarper(0.7), TopKLogitsWarper(filter_value=float('-inf'), top_k=50)])
        self.stopping_criteria = StoppingCriteriaList([MaxLengthCriteria(512)])
        self.eos_token_id = [1]
        self.record = []
        print('init finished')

    def generate_ss_mode(self, input_text, out_mq):
        if input_text == 'clear':
            print('record clear')
            self.record.clear()
            data = {
                "code": 200,
                "data": {
                    "isEnd": False,
                    "message": '聊天记录已清空'
                }
            }
            out_mq.put(data)
            return
        content = content_generate(self.record, input_text)
        inputs = self.tokenizer(text=[preprocess(content)], truncation=True, padding='max_length', max_length=768,
                                return_tensors="np")

        encoder_input_ids = inputs['input_ids']
        attention_mask = inputs['attention_mask']
        encoder_hidden_states = self.encoder.infer([encoder_input_ids, attention_mask])[0]

        print('autoregression start')
        first_loop = True
        decoder_input_ids = self.dummy_decoder_input_ids

        input_ids = torch.tensor(self.dummy_decoder_input_ids)
        unfinished_sequences = torch.tensor([1])
        while True:
            if first_loop:
                outputs = self.first_decoder.run(None, {'decoder_input_ids': decoder_input_ids,
                                                        'hidden_states': encoder_hidden_states,
                                                        'attention_mask': attention_mask})
                first_loop = False
            else:
                onnx_input = generate_onnx_input(decoder_input_ids, attention_mask, past_key_values)
                outputs = self.decoder_iter.run(None, onnx_input)
            logits = torch.tensor(outputs[0])
            past_key_values = outputs[1:]
            next_token_logits = logits[:, -1, :]

            next_token_scores = self.logits_processor(input_ids, next_token_logits)
            next_token_scores = self.logits_warper(input_ids, next_token_scores)

            probs = nn.functional.softmax(next_token_scores, dim=-1)
            next_tokens = torch.multinomial(probs, num_samples=1).squeeze(1)
            message = postprocess(self.tokenizer.batch_decode(next_tokens, skip_special_tokens=True)[0])
            if message == ' ':
                message = '&nbsp'
            elif message == '\n':
                message = '<br />'

            data = {
                "code": 200,
                "data": {
                    "isEnd": False,
                    "message": message
                }
            }

            input_ids = torch.cat([input_ids, next_tokens[:, None]], dim=-1)
            decoder_input_ids = input_ids[:, -1:].numpy()

            unfinished_sequences = unfinished_sequences.mul((sum(next_tokens != i for i in self.eos_token_id)).long())
            if unfinished_sequences.max() == 0 or self.stopping_criteria(input_ids, None):
                data['data']['isEnd'] = True
                out_mq.put(data)

                break
            else:
                out_mq.put(data)

        out_text = self.tokenizer.batch_decode(input_ids, skip_special_tokens=True)[0]
        out_text = postprocess(out_text)

        self.record.append([input_text, out_text])
        if self.tokenizer(text=[preprocess(content_generate(self.record, ''))], truncation=True, padding=True,
                          max_length=768,
                          return_tensors="np")['attention_mask'].shape[1] > 256:
            print('record clear')
            self.record.clear()
