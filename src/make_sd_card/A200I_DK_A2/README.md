## 1. 安装依赖

执行`sudo apt-get install expect parted`

![](imgs/1.png)

## 2. 步骤

1. 将该代码仓的三个脚本`compress_disk.sh`、`config.ini`、`expand_disk.sh`放入一个空的文件夹中（任意目录）。
2. 使用读卡器，将要压缩的卡插入
3. 设备名可通过`fdisk -l | grep sd`查询设备
![](imgs/2.png)
4. 执行`sudo bash ./compress_disk.sh 设备名`。根据如上图片可知，设备名为sda，有四个分区。因此执行`sudo bash ./compress_disk.sh /dev/sda`
![](imgs/3.png)

5. 执行完毕后，当前目录下的`os.img`就是压缩好、带自动扩容的镜像了
![](imgs/4.png)