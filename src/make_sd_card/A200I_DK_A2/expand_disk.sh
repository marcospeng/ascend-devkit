#!/bin/bash

# 扩展磁盘标志，若为true则执行扩展磁盘操作，执行一次后会改成false
expand_flag=true

# 默认ip和掩码
eth0_def_ip=192.168.1.100
eth1_def_ip=192.168.137.100
usb0_def_ip=192.168.2.100
def_mask=24

# 配置文件备份文件夹
conf_dir=/root/configFile
# 配置文件目标位置
conf=/exchange/config.ini

# SD卡规格表: SD卡最小大小; 1盘空间(含前置空间); 2盘空间
GB=$[1024*1024*1024]
type1=0;  type1_1=6;  type1_2=1    #  8G卡，判断标准： >  0G，分区： 6 + 1 + 剩余
type2=13; type2_1=12; type2_2=1    # 16G卡，判断标准： > 13G，分区：12 + 1 + 剩余
type3=26; type3_1=24; type3_2=1    # 32G卡，判断标准： > 26G，分区：24 + 1 + 剩余
type4=52; type4_1=48; type4_2=2    # 64G卡，判断标准： > 64G，分区：48 + 2 + 剩余
type5=104;type5_1=96; type5_2=2    #128G卡，判断标准： >104G，分区：96 + 2 + 剩余

# 磁盘信息
dev_name="/dev/mmcblk1"
if [[ $dev_name == /dev/mmc* ]]
then p1="p1"; p2="p2"; p3="p3"; p4="p4"
else p1="1";  p2="2";  p3="3";  p4="4"
fi
type_id=0

# 日志存放位置
SECLOG_DIR="/var/log/ascend_seclog"
BOOT_LOG_FILE="$SECLOG_DIR/ascend_run_servers.log"

log()
{
	cur_date=$(date "+%Y-%m-%d %H:%M:%S")
	echo "[$cur_date] [SD CARD EXPAND] "$1 >> $BOOT_LOG_FILE
}
check_soft_exist()
{
	log "check some soft does exist."
	if [[ ! -f $(type -p parted) ]]; then
		log "parted is not existed, please install it first"; return 1
	fi
	if [[ ! -f $(type -p expect) ]]; then
		log "expect is not existed, please install it first"; return 1
	fi
	if [[ ! -f $(type -p mkfs.fat) ]]; then
		log "dosfstools is not existed, please install it first"; return 1
	fi
	return 0
}
# 获得磁盘GB大小，并判断在哪个区间，拿到1、2分区大小
get_disk_capacity()
{
	log "get disk capacity."
	disk_size=$(fdisk -l | grep "${dev_name}:" | awk -F ' ' '{print $7}')
	if [[ $disk_size == "" ]]; then
		log "the device $dev_name does not exit"; return 1
	fi
	disk_size_gb=$[disk_size*512/$GB]
	for ((i=1; i <=5; i++)); do
		eval type_size=\$type$i
		if [[ $disk_size_gb -gt $type_size ]]; then type_id=$i; fi
	done
	eval disk1_size=\$[type${type_id}_1*$GB/512]
	eval disk2_size=\$[type${type_id}_2*$GB/512]
	diskfat_size=$(fdisk -l | grep $dev_name$p4 | awk -F ' ' '{print $4}')

	disk1_start=$(fdisk -l | grep $dev_name$p1 | awk -F ' ' '{print $2}')
	diskfat_start=$[disk1_start+disk1_size]
	disk2_start=$[diskfat_start+diskfat_size]
	disk3_start=$[disk2_start+disk2_size]
	disk1_end=$[diskfat_start-1]
	diskfat_end=$[disk2_start-1]
	disk2_end=$[disk3_start-1]
	disk3_end=$[disk_size-1]

	disk2_bak_size=$(fdisk -l | grep $dev_name$p2 | awk -F ' ' '{print $4}')
	disk3_bak_size=$(fdisk -l | grep $dev_name$p3 | awk -F ' ' '{print $4}')
	return 0
}
# 新增分区，传入参数为: 分区号 分区类型 起始位置 结束位置(单位: 区块)，由于仅剩余一个分区，所以创建不用选分区号
new_part()
{
	log "create the part $1 that starts at $3 and ends at $4."
	expect <<- END
	spawn fdisk $dev_name
	expect "Com*"
	send "n\n"
	expect "Sel*"
	send "p\n"
	expect "First*"
	send "$3\n"
	expect "Last*"
	send "$4\n"
	expect "Do*"
	send "Yes\n"
	expect "Com*"
	send "w\n"
	expect eof
	exit
END
	mkfs.$2 $1
	if [ $2 == "ext4" ]; then e2label $1 ubuntu_fs; fi
	log "new part create finish."
	return 0
}
# 删除分区，传入参数为：分区号
del_part()
{
	log "del part $1"
	expect <<- END
	spawn fdisk $dev_name
	expect "Com*"
	send "d\n"
	expect "Par*"
	send "$1\n"
	expect "Com*"
	send "w\n"
	expect eof
	exit
END
	log "part $1 delete finish."
	return 0
}
# 扩展分区，传入参数为: 分区号 扩展后的结束位置(单位:B)
expand_part()
{
	log "expand part $1"
	expect <<- END
	spawn parted $dev_name
	expect "parted"
	send "p\n"
	expect "parted"
	send "resizepart $1\n"
	expect "End"
	send "$2B\n"
	expect "parted"
	send "q\n"
	expect eof
	exit
END
	eval dev_now=\$dev_name\$p$1
	expect <<- END
	spawn e2fsck -f $dev_now
	expect "Fix*"
	send "y\n"
	expect eof
	exit
END
	expect <<- END
	spawn mkfs.ext4 $dev_name
	expect "Proceed*"
	send "y\n"
	expect eof
	exit
END
	resize2fs $dev_now
	log "part $1 expansion finish."
	return 0
}
# 扩展磁盘的完整过程
expand_disk()
{
	# 迁移4分区数据
	mkdir -p part4 $conf_dir
	mount $dev_name$p4 part4
	mv ./part4/* $conf_dir/
	umount part4
	rmdir part4

	# 取消挂载2、3、4盘
	if [[ $(df -h | grep $dev_name$p2) != '' ]]; then umount $dev_name$p2; fi
	if [[ $(df -h | grep $dev_name$p3) != '' ]]; then umount $dev_name$p3; fi
	if [[ $(df -h | grep $dev_name$p4) != '' ]]; then umount $dev_name$p4; fi

	# 删除4分区
	del_part 4

	# 新增4分区，把备份3分区复制到4分区，扩展4分区，删除备份3分区
	new_part $dev_name$p4 ext4 $disk3_start $[disk3_start+disk3_bak_size-1]
	dd if=$dev_name$p3 of=$dev_name$p4 bs=512 count=$disk3_bak_size
	expand_part 4 $[disk3_end*512]
	del_part 3

	# 新增3分区，把备份2分区复制到3分区，扩展3分区，删除备份2分区
	new_part $dev_name$p3 ext4 $disk2_start $[disk2_start+disk2_bak_size]
	dd if=$dev_name$p2 of=$dev_name$p3 bs=512 count=$disk2_bak_size
	expand_part 3 $[disk2_end*512]
	del_part 2

	# 新增2分区，把备份的fat分区内容移动回2分区
	new_part $dev_name$p2 fat $diskfat_start $diskfat_end
	mkdir -p /exchange
	mount $dev_name$p2 /exchange
	cp $conf_dir/* /exchange/
	umount $dev_name$p2

	# 扩展1分区
	expand_part 1 $[disk1_end*512]
	return 0
}
# 挂载磁盘
mount_disk()
{
	log "mount the /log /data /exchange ."
	mkdir -p /log /data /exchange
	# 临时挂载-每次开机挂载
	mount $dev_name$p3 /log
	mount $dev_name$p4 /data
	mount $dev_name$p2 /exchange
	# 若fat分区挂载失败，重新格式化
	if [[ $? != 0 ]]; then
		e2fsck -f -y $dev_name$p2
		mkfs.fat $dev_name$p2
	fi
	# 若fat分区内容为空，将配置文件放入
	if [[ ! -e $conf ]]; then
		cp $conf_dir/config.ini $conf
	fi
	return 0
}
# 检查、统计、扩展、挂载磁盘
expand_and_mount()
{
	check_soft_exist
	if [[ $? != 0 ]]; then return 1; fi

	get_disk_capacity
	if [[ $? != 0 ]]; then return 1; fi

	expand_disk
	if [[ $? != 0 ]]; then return 1; fi

	mount_disk
	if [[ $? != 0 ]]; then return 1; fi

	return 0
}
# 设置单个端口的ip，传入参数为：要设置的端口(eth0/eth1/usb0)
set_ip()
{
	eval dhcp4=\$$1_dhcp4
	eval address=\$$1_address
	eval mask=\$$1_mask
	eval route=\$$1_route
	eval dns_pre=\$$1_dns_pre
	eval dns_alter=\$$1_dns_alter
	echo "    $1:" >> $file
	if [[ $dhcp4 == "yes" ]]; then
		echo "      dhcp4: yes" >> $file
		log "set $1 dhcp4 to yes."
	else
		echo "      dhcp4: no" >> $file
		if [[ $address == "" ]]; then
			eval address=\$$1_def_ip
		fi
		if [[ $mask == "" ]]; then
			mask=$def_mask
		fi
		echo "      addresses: [$address/$mask]" >> $file
		log "set $1 ip=$address, mask=$mask"
	fi
	log "set $1 ip finish."
	if [[ $route != "" && $route_flag == "false" ]]; then
		echo "      routes:" >> $file
		echo "        - to: default" >> $file
		echo "          via: $route" >> $file
		route_flag=true
		log "set $1 route=$route."
	fi
	if [[ $dns_pre != "" || $dns_alter != "" ]]; then
		echo "      nameservers:" >> $file
		if [[ $dns_pre != "" ]]; then
			echo "        addresses: [$dns_pre]" >> $file
			log "set $1 dns=$dns_pre."
		fi
		if [[ $dns_alter != "" ]]; then
			echo "        addresses: [$dns_alter]" >> $file
			log "set $1 dns=$dns_alter."
		fi
	fi
	echo "" >> $file
}
# 设置两个网口和一个typeC口的ip
set_ips()
{
	# 路由是否已设置的标志
	route_flag="false"
	path="/etc/netplan"
	file_name="01-netcfg.yaml"
	file=$path/$file_name
	cd $path
	rm *.yaml
	touch $file_name
	echo "network:
  version: 2
  renderer: networkd
  ethernets:" >> $file
	set_ip eth0
	set_ip eth1
	set_ip usb0
	netplan apply
}
# 扩展磁盘和挂载部分
if [[ $expand_flag == "true" ]]; then
	expand_and_mount
	if [[ $? -ne 0 ]]; then
		log "sd card expand failed!"
	else
		log "sd card expand success!"
		echo "successful" >> /var/sd_expanded_result_flag
	fi
	sed -i "s/expand_flag=true/expand_flag=false/g" /var/expand_disk.sh
	reboot
else
	mount_disk
fi
# 设置ip部分
if [ -e $conf ]; then
	chmod +x $conf
	dos2unix $conf
	. $conf
	if [[ $setting_flag == "true" ]]; then
		set_ips
		sed -i "s/^setting_flag=true$/setting_flag=false/g" $conf
		log "setting ip finish."
	else
		log "the ip does not need to be set."
	fi
fi