#!/bin/bash

dev_name=$1
file_name=${BASH_SOURCE[0]}
path=$(cd "$(dirname $file_name)" && pwd)
expand_file="$path/expand_disk.sh"
config_file="$path/config.ini"
exd=exchange_dir
fat_size=$[50*1024*2]

log()
{
	cur_date=$(date "+%Y-%m-%d %H:%M:%S")
	echo "[$cur_date] [SD CARD EXPAND] "$1
}

# 获得磁盘信息
get_disk_val()
{
	if [[ $(fdisk -l | grep "$dev_name:") == '' ]]; then
		log "Error! the device $dev_name does not exit!" ; return 1
	fi
	
	dev_size=$(fdisk -l | grep "$dev_name:" | awk -F ' ' '{print $5}')
	dev_sec=$(fdisk -l | grep "$dev_name:" | awk -F ' ' '{print $7}')
	log "dev_name is $dev_name, size is $dev_size, have $dev_sec sector."
	if [[ $dev_name == /dev/mmc* ]]; then
		p1="p1"; p2="p2"; p3="p3"; p4="p4"
	else
		p1="1";  p2="2";  p3="3"; p4="4"
	fi
	parts=($(blkid | grep $dev_name | grep ext4 | cut -d ':' -f 1 | sort))
	part_fat=($(blkid | grep $dev_name | grep fat | cut -d ':' -f 1))
	part1=${parts[0]}
	part2=${parts[1]}
	part3=${parts[2]}
	log "this device have ext4 part $part1, $part2, $part3."
	return 0
}

# 压缩分区，输入参数为： 分区名 起始分区 额外空间(单位MB)
compress_ext4_part()
{
	if [[ $(df -h | grep $1) != '' ]]; then umount $1; fi
	base_size=$(fdisk -l | grep $1 | awk -F ' ' '{print $4}')
	e2fsck -f -y $1
	block_num=$(resize2fs -P $1 | cut -d ' ' -f 7)
	if [[ "$block_num" == "" ]]; then return 0; fi
	target_size=$[block_num*4+4+$3*1024]
	if [ $[target_size*2] -gt $base_size ]; then return 0; fi
	target_end=$[target_size*1024+$2*512-512]
	dev_id=${1: -1}
	e2fsck -f -y $1
	resize2fs -p $1 ${target_size}K
	expect <<- END
	spawn parted $dev_name
	expect "(parted)*"
	send "resizepart $dev_id\n"
	expect "End*"
	send "${target_end}B\n"
	expect "Yes*"
	send "Yes\n"
	expect "(parted)*"
	send "q\n"
	expect eof
	exit
END
	partprobe
}

# 获得各个分区的头信息
get_part_head()
{
	sd1_begin=$(fdisk -l | grep $part1 | awk -F ' ' '{print $2}')
	sd2_begin=$(fdisk -l | grep $part2 | awk -F ' ' '{print $2}')
	sd3_begin=$(fdisk -l | grep $part3 | awk -F ' ' '{print $2}')
	log "the begin of $part1: $sd1_begin, $part2: $sd2_begin, $part3: $sd3_begin." 
}


# 获得各个分区的结尾和压缩后大小信息
get_part_size()
{
	sd1_end=$(  fdisk -l | grep $part1 | awk -F ' ' '{print $3}')
	sd1_size=$( fdisk -l | grep $part1 | awk -F ' ' '{print $4}')
	sd2_end=$(  fdisk -l | grep $part2 | awk -F ' ' '{print $3}')
	sd2_size=$( fdisk -l | grep $part2 | awk -F ' ' '{print $4}')
	sd3_end=$(  fdisk -l | grep $part3 | awk -F ' ' '{print $3}')
	sd3_size=$( fdisk -l | grep $part3 | awk -F ' ' '{print $4}')
	log "the size of sd card is $[dev_sec/2048]MB, it will be compress to $[(sd1_size+sd2_size+sd3_size)/2048]MB."
}


# 删除分区，输入参数为： 分区号
del_part()
{
	log "del part $1"
	expect <<- END
	spawn fdisk $dev_name
	expect "Com*"
	send "d\n"
	expect "Par*"
	send "$1\n"
	expect "Com*"
	send "w\n"
	expect eof
	exit
END
	partprobe
}

# 创建新的分区保存原有分区数据，输入参数为： 分区号 分区类型 起始位置 结束位置
create_part()
{
	log "create the new part $1."
	expect <<- END
	spawn fdisk $dev_name
	expect "Com*"
	send "n\n"
	expect "Sel*"
	send "p\n"
	expect "Par*"
	send "$1\n"
	expect "First*"
	send "$3\n"
	expect "Last*"
	send "$4\n"
	expect "Do*"
	send "Yes\n"
	expect "Com*"
	send "w\n"
	expect eof
	exit
END
	partprobe
	eval part_name=$dev_name\$p$1
	expect <<- END
	spawn mkfs.$2 $part_name
	expect "Process*"
	send "y\n"
	expect eof
	exit
END
	partprobe
	e2label $part_name ubuntu_fs
}

# 备份2、3盘并取消挂载，若有fat盘则备份config.ini文件，并删除fat盘
backup_part()
{
	log "begin to backup the $part2 and $part3."
	dd if=$part2 of=sd2.img bs=512 count=$sd2_size
	dd if=$part3 of=sd3.img bs=512 count=$sd3_size
	
	if [[ $part_fat != "" ]]; then
		if [[ $(df -h | grep $part_fat) != '' ]]; then umount $part_fat; fi
		mount $part_fat $exd
		if [[ -e $exd/config.ini ]]; then
			log "Backup config.ini to config.ini.base, you can copy the config.ini.base to $part_fat to use the old config.ini"
			mv $exd/config.ini ${config_file}.base
		fi
		umount $part_fat
		del_part ${part_fat: -1}
	fi
	log "backup finish."
}

saving()
{
	log "write the data to new part 2 and part 3."
	# 将备份的数据保存至新的分区
	dd if=sd2.img of=$dev_name$p2 bs=512 count=$sd2_size
	dd if=sd3.img of=$dev_name$p3 bs=512 count=$sd3_size
	rm sd2.img sd3.img
	# 复制expand_disk 和 config.ini，配置自动扩容功能
	log "setting the automatic partition expansion function."
	mount $part1 $exd
	cp $expand_file $exd/var/
	sed -i "s?exit 0??" $exd/etc/rc.local
	if [[ "$(cat $exd/etc/rc.local | grep expand_disk.sh)" == "" ]]; then
		echo "if [ -e /var/expand_disk.sh ]; then
	chmod +x /var/expand_disk.sh
	/bin/bash /var/expand_disk.sh >> /root/expand.log
fi
exit 0
" >> $exd/etc/rc.local
	fi
	umount $exd
	mount $dev_name$p4 $exd
	cp $config_file $exd
	umount $exd
	
	log "begin to backup the sd card."
	# 备份sd卡中现有的四个盘和前置部分
	dd if=$dev_name of=sd.img bs=512 count=$[sd1_end+sd2_size+sd3_size+fat_size]
	log "backup finish. the image file is sd.img."
}

main()
{
	if [[ $dev_name == "" ]]; then
		log "Please run like \"./compress_disk.sh /dev/sdb\"!" ; return 1
	fi
	if [ ! -e $expand_file ]; then
		echo "The expand_disk.sh file does not exist."; return 1
	fi
	if [ ! -e $config_file ]; then
		echo "The config.ini file does not exist."; return 1
	fi
	get_disk_val
	if [[ $? != 0 ]]; then return 1; fi

	mkdir -p $exd
	get_part_head
	compress_ext4_part $part1 $sd1_begin 500
	compress_ext4_part $part2 $sd2_begin 0
	compress_ext4_part $part3 $sd3_begin 0
	get_part_size
	backup_part
	del_part ${part2: -1}
	del_part ${part3: -1}
	create_part 2 ext4 $[sd1_end+1]                   $[sd1_end+sd2_size]
	create_part 3 ext4 $[sd1_end+sd2_size+1]          $[sd1_end+sd2_size+sd3_size]
	create_part 4 fat  $[sd1_end+sd2_size+sd3_size+1] $[sd1_end+sd2_size+sd3_size+fat_size]
	saving
	rmdir $exd
}

main
if [[ $? -ne 0 ]]; then
	log "sd card compress failed!"
else
	log "sd card compress success!"
fi